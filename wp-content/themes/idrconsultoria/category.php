<?php get_header(); ?>

    <div id="faixa-interna">
        <div class="container">Blog</div>
    </div>
    <div id="breadcrumb">
        <div class="container">idr > pagina</div>
    </div>
    <div id="container" class="container">
        <div id="content">

            
        <!-- #categorias -->
        <div id="categorias" class="container">
            <div id="escolha">
                <div id="escolha-label">ESCOLHA SEU ASSUNTO</div>
                <ul class="simple-tabs" id="escolha-categorias">
                    <li class="categoria-1 active">Financiamento<span class="divisor"></span></li>
                    <li class="categoria-2">Categoria 2<span class="divisor"></span></li>
                    <li class="categoria-3">Categoria 3<span class="divisor"></span></li>
                    <li class="categoria-4">Categoria 4</li>
                </ul>
            </div>
            <div class="clearfix"></div>

            <!-- .tabe-page -->
            <div id="categoria-1" class="tab-page active-page">
                <!-- .cat-item -->
                <div class="cat-item">
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-1.png" />
                    <div class="cat-item-text">
                        <div class="categoria">CATEGORIA</div>
                        <div class="titulo">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat</div>
                        <div class="texto">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida
                            dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.
                            Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni
                            dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia
                            dolor sit amet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                        </div>
                        <div class="link"><a href="javascript:;">LER MAIS</a></div>
                    </div>
                </div>
                <!-- /.cat-item -->
                <!-- .cat-item -->
                <div class="cat-item">
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-2.png" />
                    <div class="cat-item-text">
                        <div class="categoria">CATEGORIA</div>
                        <div class="titulo">Proin sodales pulvinar tempor voluptas sit aut odit</div>
                        <div class="texto">
                            Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid
                            ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil
                            molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur. Nemo enim ipsam
                            voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui
                            ratione voluptatem sequi nesciunt.
                        </div>
                        <div class="link"><a href="javascript:;">LER MAIS</a></div>
                    </div>
                </div>
                <!-- /.cat-item -->
                <!-- .cat-item -->
                <div class="cat-item">
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-3.png" />
                    <div class="cat-item-text">
                        <div class="categoria">CATEGORIA</div>
                        <div class="titulo">Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam</div>
                        <div class="texto">
                            Etiam vel odio in dolor ultricies pretium. Praesent diam massa, pharetra vel ex sit amet, cursus porttitor dolor.
                            Quisque et nunc id ex lobortis pretium a sed quam. Morbi nec nulla sed orci lacinia fermentum eget sed tellus.
                            Nullam at nunc turpis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                            Ut hendrerit quis magna quis pellentesque. Fusce volutpat feugiat mauris et gravida. Proin euismod ultrices lacus,
                            eu rhoncus ante interdum ut. Praesent eget rutrum turpis. Donec nec semper mauris. Donec a egestas tellus.
                            Aliquam erat dolor, vehicula ac faucibus a, aliquet sed libero.
                        </div>
                        <div class="link"><a href="javascript:;">LER MAIS</a></div>
                    </div>
                </div>
                <!-- /.cat-item -->
            </div>
            <!-- /.tabe-page -->

            <!-- .tabe-page -->
            <div id="categoria-2" class="tab-page">
                <!-- .cat-item -->
                <div class="cat-item">
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-1.png" />
                    <div class="cat-item-text">
                        <div class="categoria">CATEGORIA</div>
                        <div class="titulo">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat</div>
                        <div class="texto">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida
                            dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.
                            Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni
                            dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia
                            dolor sit amet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                        </div>
                        <div class="link"><a href="javascript:;">LER MAIS</a></div>
                    </div>
                </div>
                <!-- /.cat-item -->
                <!-- .cat-item -->
                <div class="cat-item">
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-1.png" />
                    <div class="cat-item-text">
                        <div class="categoria">CATEGORIA</div>
                        <div class="titulo">Proin sodales pulvinar tempor voluptas sit aut odit</div>
                        <div class="texto">
                            Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid
                            ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil
                            molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur. Nemo enim ipsam
                            voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui
                            ratione voluptatem sequi nesciunt.
                        </div>
                        <div class="link"><a href="javascript:;">LER MAIS</a></div>
                    </div>
                </div>
                <!-- /.cat-item -->
                <!-- .cat-item -->
                <div class="cat-item">
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-1.png" />
                    <div class="cat-item-text">
                        <div class="categoria">CATEGORIA</div>
                        <div class="titulo">Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam</div>
                        <div class="texto">
                            Etiam vel odio in dolor ultricies pretium. Praesent diam massa, pharetra vel ex sit amet, cursus porttitor dolor.
                            Quisque et nunc id ex lobortis pretium a sed quam. Morbi nec nulla sed orci lacinia fermentum eget sed tellus.
                            Nullam at nunc turpis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                            Ut hendrerit quis magna quis pellentesque. Fusce volutpat feugiat mauris et gravida. Proin euismod ultrices lacus,
                            eu rhoncus ante interdum ut. Praesent eget rutrum turpis. Donec nec semper mauris. Donec a egestas tellus.
                            Aliquam erat dolor, vehicula ac faucibus a, aliquet sed libero.
                        </div>
                        <div class="link"><a href="javascript:;">LER MAIS</a></div>
                    </div>
                </div>
                <!-- /.cat-item -->
            </div>
            <!-- /.tabe-page -->

            <!-- .tabe-page -->
            <div id="categoria-3" class="tab-page">
                <!-- .cat-item -->
                <div class="cat-item">
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-2.png" />
                    <div class="cat-item-text">
                        <div class="categoria">CATEGORIA</div>
                        <div class="titulo">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat</div>
                        <div class="texto">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida
                            dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.
                            Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni
                            dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia
                            dolor sit amet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                        </div>
                        <div class="link"><a href="javascript:;">LER MAIS</a></div>
                    </div>
                </div>
                <!-- /.cat-item -->
                <!-- .cat-item -->
                <div class="cat-item">
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-2.png" />
                    <div class="cat-item-text">
                        <div class="categoria">CATEGORIA</div>
                        <div class="titulo">Proin sodales pulvinar tempor voluptas sit aut odit</div>
                        <div class="texto">
                            Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid
                            ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil
                            molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur. Nemo enim ipsam
                            voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui
                            ratione voluptatem sequi nesciunt.
                        </div>
                        <div class="link"><a href="javascript:;">LER MAIS</a></div>
                    </div>
                </div>
                <!-- /.cat-item -->
                <!-- .cat-item -->
                <div class="cat-item">
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-2.png" />
                    <div class="cat-item-text">
                        <div class="categoria">CATEGORIA</div>
                        <div class="titulo">Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam</div>
                        <div class="texto">
                            Etiam vel odio in dolor ultricies pretium. Praesent diam massa, pharetra vel ex sit amet, cursus porttitor dolor.
                            Quisque et nunc id ex lobortis pretium a sed quam. Morbi nec nulla sed orci lacinia fermentum eget sed tellus.
                            Nullam at nunc turpis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                            Ut hendrerit quis magna quis pellentesque. Fusce volutpat feugiat mauris et gravida. Proin euismod ultrices lacus,
                            eu rhoncus ante interdum ut. Praesent eget rutrum turpis. Donec nec semper mauris. Donec a egestas tellus.
                            Aliquam erat dolor, vehicula ac faucibus a, aliquet sed libero.
                        </div>
                        <div class="link"><a href="javascript:;">LER MAIS</a></div>
                    </div>
                </div>
                <!-- /.cat-item -->
            </div>
            <!-- /.tabe-page -->

            <!-- .tabe-page -->
            <div id="categoria-4" class="tab-page">
                <!-- .cat-item -->
                <div class="cat-item">
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-3.png" />
                    <div class="cat-item-text">
                        <div class="categoria">CATEGORIA</div>
                        <div class="titulo">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat</div>
                        <div class="texto">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida
                            dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.
                            Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni
                            dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia
                            dolor sit amet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                        </div>
                        <div class="link"><a href="javascript:;">LER MAIS</a></div>
                    </div>
                </div>
                <!-- /.cat-item -->
                <!-- .cat-item -->
                <div class="cat-item">
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-3.png" />
                    <div class="cat-item-text">
                        <div class="categoria">CATEGORIA</div>
                        <div class="titulo">Proin sodales pulvinar tempor voluptas sit aut odit</div>
                        <div class="texto">
                            Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid
                            ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil
                            molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur. Nemo enim ipsam
                            voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui
                            ratione voluptatem sequi nesciunt.
                        </div>
                        <div class="link"><a href="javascript:;">LER MAIS</a></div>
                    </div>
                </div>
                <!-- /.cat-item -->
                <!-- .cat-item -->
                <div class="cat-item">
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-3.png" />
                    <div class="cat-item-text">
                        <div class="categoria">CATEGORIA</div>
                        <div class="titulo">Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam</div>
                        <div class="texto">
                            Etiam vel odio in dolor ultricies pretium. Praesent diam massa, pharetra vel ex sit amet, cursus porttitor dolor.
                            Quisque et nunc id ex lobortis pretium a sed quam. Morbi nec nulla sed orci lacinia fermentum eget sed tellus.
                            Nullam at nunc turpis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                            Ut hendrerit quis magna quis pellentesque. Fusce volutpat feugiat mauris et gravida. Proin euismod ultrices lacus,
                            eu rhoncus ante interdum ut. Praesent eget rutrum turpis. Donec nec semper mauris. Donec a egestas tellus.
                            Aliquam erat dolor, vehicula ac faucibus a, aliquet sed libero.
                        </div>
                        <div class="link"><a href="javascript:;">LER MAIS</a></div>
                    </div>
                </div>
                <!-- /.cat-item -->
            </div>
            <!-- /.tabe-page -->

        </div>
        <!-- /#categorias -->
            
            <?php global $wp_query; $total_pages = $wp_query->max_num_pages; if ( $total_pages > 1 ) { ?>
            <div id="nav-below" class="navigation">
                <div class="nav-previous"><?php next_posts_link(__( '<span class="meta-nav">«</span> Older posts', 'your-theme' )) ?></div>
                <div class="nav-next"><?php previous_posts_link(__( 'Newer posts <span class="meta-nav">»</span>', 'your-theme' )) ?></div>
            </div><!-- #nav-below -->
            <?php } ?>

        </div><!-- #content -->
    </div><!-- #container -->
 
<?php get_sidebar(); ?>
<?php get_footer(); ?>