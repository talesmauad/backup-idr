<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
    <title><?php
    if ( is_single() ) { single_post_title(); }
    elseif ( is_home() || is_front_page() ) { bloginfo('name'); print ' | '; bloginfo('description'); get_page_number(); }
    elseif ( is_page() ) { single_post_title(''); }
    elseif ( is_search() ) { bloginfo('name'); print ' | Resultados da pesquisa para ' . wp_specialchars($s); get_page_number(); }
    elseif ( is_404() ) { bloginfo('name'); print ' | Não encontrado'; }
    else { bloginfo('name'); wp_title('|'); get_page_number(); }
    ?></title>
    <meta http-equiv="content-type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-site-verification" content="H1iMGyDXSvMFf_c7LAxXv0Lzqk5R21F0HlrYkf5OBj8" />
    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:400, 700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Unna" rel="stylesheet">
    <!-- css -->
    <link rel="stylesheet" type="text/css" href="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/css/reset.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/css/slicknav.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/css/responsive.css" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/idrloggo.png" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" />
    <?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
    <?php wp_head(); ?>
    <link rel="alternate" type="application/rss+xml" href="<?php bloginfo('rss2_url'); ?>" title="<?php printf( __( '%s latest posts', 'your-theme' ), wp_specialchars( get_bloginfo('name'), 1 ) ); ?>" />
    <link rel="alternate" type="application/rss+xml" href="<?php bloginfo('comments_rss2_url') ?>" title="<?php printf( __( '%s latest comments', 'your-theme' ), wp_specialchars( get_bloginfo('name'), 1 ) ); ?>" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <!-- Smartsupp Live Chat script -->
    <script type="text/javascript">
        var _smartsupp = _smartsupp || {};
        _smartsupp.key = 'ba9d46ac0edb2547d7e1c10106705852d31f0abb';
        window.smartsupp||(function(d) {
            var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
            s=d.getElementsByTagName('script')[0];c=d.createElement('script');
            c.type='text/javascript';c.charset='utf-8';c.async=true;
            c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
        })(document);
    </script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-68024111-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-68024111-1');
</script>

</head>
 
<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
    <div id="header" class="container">
        <nav>
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- logo -->
                    <a class="navbar-brand" href="<?php echo get_site_url(); ?>">
                        <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/logo-idr-consultoria.png" />
                    </a>
                    <!-- /logo -->
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <div class="pull-right">
                        <ul id="first-menu" class="nav navbar-nav">
                            <li class="tel">
                                <a href="callto:1150878957">(11) 5087-8957</a>
                                <span class="divisor"></span>
                            </li>
                            <li>
                                <a href="<?php echo get_site_url(); ?>/blog">Blog</a>
                                <span class="divisor"></span>
                            </li>
                            <li>
                                <a href="<?php echo get_site_url(); ?>/cursos">Cursos</a>
                            </li>
                            <li>
                                <a href="<?php echo get_site_url(); ?>/contato" class="btn-consultor">Fale com um consultor</a>
                            </li>
                        </ul>
                    </div><!-- .pull-right -->
                    <div class="clearfix"></div><!-- .cleardix -->
                    <div class="pull-right">
                        <ul id="second-menu" class="nav navbar-nav">
                            <li class="tirar-desk"><a href="<?php echo get_site_url(); ?>">Home</a></li>
                            <li class="dropdown">
                                <a href="<?php echo get_permalink( 76 ); ?>" class="but-um dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Financiamento <span class="caret"></span>
                                </a>
                                <ul id="submenu1" class="dropdown-menu submenu">
                                    <li><a class="active" href="<?php echo get_permalink( 78 ); ?>">BNDES</a></li>
                                    <li><a href="<?php echo get_permalink( 84 ); ?>">Inovacred e MPME</a></li>
                                    <li><a href="<?php echo get_permalink( 82 ); ?>">Desenvolve SP</a></li>
                                    <li><a href="<?php echo get_permalink( 86 ); ?>">Outros Bancos Públicos</a></li>
                                    <li><a href="<?php echo get_permalink( 88 ); ?>">Finep 30 dias</a></li>
                                    <li><a href="<?php echo get_permalink( 90 ); ?>">Bancos Privados</a></li>
                                    <li class="destaque">
                                        <div class="submenu-title">empréstimos</div>
                                        <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/submenu-financiamento.png" />
                                        <p>A IDR auxilia a sua empresa a identificar e captar o melhor financiamento possível dentre diversas opções de mercado de bancos públicos e instituições financeiras privadas.</p>
                                    </li>
                                </ul><!-- submenu1 -->
                                <span class="divisor"></span>
                            </li>
                            <li class="dropdown">
                                <a href="<?php echo get_permalink( 92 ); ?>" class="but-dois dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Fundo Perdido <span class="caret"></span>
                                </a>
                                <ul id="submenu2" class="dropdown-menu submenu">
                                    <li><a class="active" href="<?php echo get_permalink( 94 ); ?>">Fapesp Pipe</a></li>
                                    <li><a href="<?php echo get_permalink( 96 ); ?>">Cnpq - Rhae</a></li>
                                    <li><a href="<?php echo get_permalink( 98 ); ?>">Finep</a></li>
                                    <li><a href="<?php echo get_permalink( 100 ); ?>">Outros</a></li>
                                    <li class="destaque">
                                        <div class="submenu-title">fundo perdido</div>
                                        <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/submenu-fundo-perdido.png" />
                                        <p>A idr possui equipe de consultores altamente especializados para auxiliar sua empresa na estruturação e elaboração de projetos para órgãos como FAPESP, FINEP, BNDES Funtec, CNPq, entre outros.</p>
                                    </li>
                                </ul>
                                <span class="divisor"></span>
                            </li>
                            <li class="dropdown">
                                <a href="<?php echo get_permalink( 103 ); ?>" class="but-tres dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    M&amp;A <span class="caret"></span>
                                </a>
                                <ul id="submenu3" class="dropdown-menu submenu">
                                    <li><a class="active" href="<?php echo get_permalink( 109 ); ?>">Captação de Investidor</a></li>
                                    <li><a href="<?php echo get_permalink( 105 ); ?>">Plano de negócio e valuation</a></li>
                                    <li><a href="<?php echo get_permalink( 480 ); ?>">Governança</a></li>
                                    <li><a href="<?php echo get_permalink( 107 ); ?>">Controladoria e gestão financeira</a></li>
                                    <li class="destaque">
                                        <div class="submenu-title">captação de investimento</div>
                                        <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/submenu-mea.png" />
                                        <p>A idr possui uma equipe de Advisory board altamente especializada na estruturação e viabilização de operações de M&amp;A, Venture Capital e Angel Capital.</p>
                                    </li>
                                </ul>
                                <span class="divisor"></span>
                            </li>
                            <li class="dropdown">
                                <a href="<?php echo get_permalink( 111 ); ?>" class="but-quatro dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Lei do Bem <span class="caret"></span>
                                </a>
                                <ul id="submenu4" class="dropdown-menu submenu">
                                    <li><a class="active" href="<?php echo get_permalink( 113 ); ?>">Lei do Bem</a></li>
                                    <li><a href="<?php echo get_permalink( 115 ); ?>">Revisão Fiscal</a></li>
                                    <li class="destaque">
                                        <div class="submenu-title">incentivos fiscais</div>
                                        <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/submenu-lei-do-bem.png" />
                                        <p>A idr auxilia empresas de diversos portes e setores a operacionalizar e utilizar os incentivos fiscais da Lei do Bem, Lei da Informática e também na otimização tributária e fiscal.</p>
                                    </li>
                                </ul>
                            </li>
                            <li class="tirar-desk"><a href="<?php echo get_site_url(); ?>/blog">Blog</a></li>
                            <li class="tirar-desk"><a href="<?php echo get_site_url(); ?>/cursos">Cursos</a></li>
                            <li class="tirar-desk"><a href="<?php echo get_site_url(); ?>/faq/como-conseguir-um-financiamento-para-inovacao-em-sc/">Faq</a></li>
                            <li class="tirar-desk fale-consult-evidencia"><a href="<?php echo get_site_url(); ?>/contato">Fale com um consultor</a></li>
                        </ul>
                    </div>
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>
    </div>
    <div id="main">
