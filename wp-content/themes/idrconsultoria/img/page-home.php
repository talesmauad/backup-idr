<?php
/**
 * Template Name: Página Home
 *
 * @package WordPress
 * @subpackage IDR Consultoria
 */

/* Advanced Custom Fields */
/* Banner Principal */
$imagem_banner_esquerda         = get_field('imagem_banner_esquerda');
$imagem_banner_direita          = get_field('imagem_banner_direita');
$texto_banner_esquerda          = get_field('texto_banner_esquerda');

$pergunta_1                     = get_field('pergunta_1');
$opcao_1_1                      = get_field('opcao_1_1');
$opcao_1_2                      = get_field('opcao_1_2');
$opcao_1_3                      = get_field('opcao_1_3');
$opcao_1_4                      = get_field('opcao_1_4');

$pergunta_2                     = get_field('pergunta_2');
$opcao_2_1                      = get_field('opcao_2_1');
$opcao_2_2                      = get_field('opcao_2_2');
$opcao_2_3                      = get_field('opcao_2_3');
$opcao_2_4                      = get_field('opcao_2_4');

$pergunta_3                     = get_field('pergunta_3');
$opcao_3_1                      = get_field('opcao_3_1');
$opcao_3_2                      = get_field('opcao_3_2');
$opcao_3_3                      = get_field('opcao_3_3');
$opcao_3_4                      = get_field('opcao_3_4');

/* Como Funciona */
$como_funciona_titulo_1         = get_field('como_funciona_titulo_1');
$como_funciona_texto_1          = get_field('como_funciona_texto_1');
$como_funciona_titulo_2         = get_field('como_funciona_titulo_2');
$como_funciona_texto_2          = get_field('como_funciona_texto_2');
$como_funciona_titulo_3         = get_field('como_funciona_titulo_3');
$como_funciona_texto_3          = get_field('como_funciona_texto_3');
$como_funciona_titulo_4         = get_field('como_funciona_titulo_4');
$como_funciona_texto_4          = get_field('como_funciona_texto_4');

/* Dicas e Tutoriais */
$dicas_e_tutoriais_texto        = get_field('dicas_e_tutoriais_texto');

/* Serviços */
$fundo_perdido_texto            = get_field('fundo_perdido_texto');
$lei_do_bem_texto               = get_field('lei_do_bem_texto');
$mea_texto                      = get_field('mea_texto');

/*IDR em Números*/
$direita_em_num                 = get_field('direita_em_num');
$centro_em_num                  = get_field('centro_em_num');
$esquerda_em_num                = get_field('esquerda_em_num');


?> 


    <?php get_header(); ?>

    <!-- #main-banner -->
    <div id="main-banner">
        <div class="esq" style="background: url(<?php echo $imagem_banner_esquerda['url']; ?>) no-repeat;background-size:cover;">
            <div class="field">
                <?php echo $texto_banner_esquerda; ?>
                <a class="bt-download" href="http://material.idrconsultoria.com.br/landing-page-guia-definitivo-do-financiamento" target="_blank"></a>
            </div>
        </div>
        <div class="dir" style="background: url(<?php echo $imagem_banner_direita['url']; ?>) no-repeat;background-size:cover;">
            <div class="container-pergunta" data-target="#capital">
                <div class="pergunta"><?php echo $pergunta_1 ?></div>
                <div class="opcao opcao1 active" data-value="Até R$1 milhão"><div><?php echo $opcao_1_1 ?></div></div>
                <div class="opcao opcao1" data-value="De R$ 1 a 5 milhões"><div><?php echo $opcao_1_2 ?></div></div>
                <div class="opcao opcao1" data-value="De R$ 5 a 10 milhões"><div><?php echo $opcao_1_3 ?></div></div>
                <div class="opcao opcao1" data-value="Acima de 10 milhões"><div><?php echo $opcao_1_4 ?></div></div>
                <div class="clearfix"></div>
                <div class="error">Selecione uma opção</div>
            </div>
            <div class="container-pergunta" data-target="#faturamento">
                <div class="pergunta"><?php echo $pergunta_2 ?></div>
                <div class="opcao opcao2 active" data-value="Até R$ 1 milhão"><div><?php echo $opcao_2_1 ?></div></div>
                <div class="opcao opcao2" data-value="De R$ 1 a 10 milhões"><div><?php echo $opcao_2_2 ?></div></div>
                <div class="opcao opcao2" data-value="De R$ 10 a 100 milhões"><div><?php echo $opcao_2_3 ?></div></div>
                <div class="opcao opcao2" data-value="Acima de 100 milhões"><div><?php echo $opcao_2_4 ?></div></div>
                <div class="clearfix"></div>
                <div class="error">Selecione uma opção</div>
            </div>
            <div class="container-pergunta" data-target="#destino">
                <div class="pergunta"><?php echo $pergunta_3 ?></div>
                <div class="opcao opcao3 active" data-value="Capital de giro"><div><?php echo $opcao_3_1 ?></div></div>
                <div class="opcao opcao3" data-value="Ampliação"><div><?php echo $opcao_3_2 ?></div></div>
                <div class="opcao opcao3" data-value="Novos projetos"><div><?php echo $opcao_3_3 ?></div></div>
                <div class="opcao opcao3" data-value="Tecnologia"><div><?php echo $opcao_3_4 ?></div></div>
                <div class="clearfix"></div>
                <div class="error">Selecione uma opção</div>
            </div>
            <a href="" id="main-banner-submit" data-toggle="modal" data-target="#myModa1"></a>
        </div>
    </div>
    <!-- /#main-banner -->
    <div id="alternate-banner" style="background: url(<?php echo $imagem_banner_direita['url']; ?>) no-repeat bottom left;background-size:cover;">
        <div class="partedafoto"><img src="<?php echo $imagem_banner_esquerda['url']; ?>"></div>
        <div class="field">
            <?php echo $texto_banner_esquerda; ?>
            <a class="bt-download" href="http://material.idrconsultoria.com.br/landing-page-guia-definitivo-do-financiamento" target="_blank"></a>       
        </div>
        <div class="" id="myModa2" tabindex="-1">
          <div class="" role="">
            <div class="">
              <div class="">
                <h4 class="modal-title text-center" style="font-size: 1.5em !important;font-weight: bold;" id="myModalLabel">Deixe seu contato e enviaremos as melhores opções de financiamento para a sua empresa</h4>
              </div>
              <div class="modal-body">
                <?php echo do_shortcode( '[contact-form-7 id="756" title="Financiamento2"]' ); ?>
              </div>
            </div>
          </div>
        </div>
    </div>
    <!-- #como-funciona -->
    <div id="como-funciona" class="container">
        <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h3>COMO FUNCIONA</h3>
                <div class="hr-border"></div>
            </div>
        </div>
        <div class="row steps">
            <div id="step1" class="col-lg-3 step">
                <h2><span class="number">1.</span><?php echo $como_funciona_titulo_1 ?></h2>
                <p><?php echo $como_funciona_texto_1 ?></p>
            </div>
            <div id="step2" class="col-lg-3 step">
                <h2><span class="number">2.</span><?php echo $como_funciona_titulo_2 ?></h2>
                <p><?php echo $como_funciona_texto_2 ?></p>
            </div>
            <div id="step3" class="col-lg-3 step">
                <h2><span class="number">3.</span><?php echo $como_funciona_titulo_3 ?></h2>
                <p><?php echo $como_funciona_texto_3 ?></p>
            </div>
            <div id="step4" class="col-lg-3 step">
                <h2><span class="number">4.</span><?php echo $como_funciona_titulo_4 ?></h2>
                <p><?php echo $como_funciona_texto_4 ?></p>
            </div>

        </div>
        </div>
    </div>
    <!-- /#como-funciona -->

    <!-- .bg-escuro -->
    <div class="bg-escuro">
        <!-- #dicas -->
        <div id="dicas" class="container">
            <div class="row">
                <div class="col-lg-12">
                <h3>DICAS E TUTORIAIS</h3>
                <div class="hr-border"></div>
                </div>
            </div>
            <div class="row">
                <div id="dicas-desc" class="col-lg-12">
                    <?php echo $dicas_e_tutoriais_texto; ?>
                </div>
                <?php
                $args = array(
                    'post_type' => 'post',
                    'orderby' => 'post_date',
                    'posts_per_page' => 1,
                    'cat' => 3,
                );
                $the_query = new WP_Query( $args );
                $i = 1;
                $not_in = array();

                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    $meta = get_post_meta( $post->ID, 'custom_fields', true );
                    $not_in[] = $post->ID;
                ?>
                    <div class="col-md-4">
                        <div class="dica text-center">
                            <?php
                            if ( has_post_thumbnail() ) {
                                the_post_thumbnail( 'dicas_tutoriais' );
                            } else {
                            ?>
                                <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/dica-<?php echo $i; ?>.png" />
                            <?php
                            }
                            ?>
                            <div class="bg-dica">
                                <div class="categoria">
                                    <?php
                                    // Obter as categorias
                                    $categories = get_the_category();
                                    // Verificar se existem categorias
                                    if ( ! empty( $categories ) ) {
                                        // Obter a primeira categoria
                                        $categorie = current($categories);
                                        echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                                    }
                                    ?>
                                </div>
                                <div class="titulo">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </div>
                                <p>
                                    <?php echo $meta['texto_destaque']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php $i++; endwhile; endif; wp_reset_postdata(); ?>
                <?php
                $args = array(
                    'post_type' => 'post',
                    'orderby' => 'post_date',
                    'posts_per_page' => 1,
                    'cat' => '6',
                );
                $the_query = new WP_Query( $args );
                $i = 1;
                $not_in = array();

                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    $meta = get_post_meta( $post->ID, 'custom_fields', true );
                    $not_in[] = $post->ID;
                ?>
                    <div class="col-md-4">
                        <div class="dica">
                            <?php
                            if ( has_post_thumbnail() ) {
                                the_post_thumbnail( 'dicas_tutoriais' );
                            } else {
                            ?>
                                <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/dica-<?php echo $i; ?>.png" />
                            <?php
                            }
                            ?>
                            <div class="bg-dica">
                                <div class="categoria">
                                    <?php
                                    // Obter as categorias
                                    $categories = get_the_category();
                                    // Verificar se existem categorias
                                    if ( ! empty( $categories ) ) {
                                        // Obter a primeira categoria
                                        $categorie = current($categories);
                                        echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                                    }
                                    ?>
                                </div>
                                <div class="titulo">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </div>
                                <p>
                                    <?php echo $meta['texto_destaque']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php $i++; endwhile; endif; wp_reset_postdata(); ?>
                <?php
                $args = array(
                    'post_type' => 'post',
                    'orderby' => 'post_date',
                    'posts_per_page' => 1,
                    'cat' => 5,
                );
                $the_query = new WP_Query( $args );
                $i = 1;
                $not_in = array();

                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    $meta = get_post_meta( $post->ID, 'custom_fields', true );
                    $not_in[] = $post->ID;
                ?>
                    <div class="col-md-4">
                        <div class="dica">
                            <?php
                            if ( has_post_thumbnail() ) {
                                the_post_thumbnail( 'dicas_tutoriais' );
                            } else {
                            ?>
                                <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/dica-<?php echo $i; ?>.png" />
                            <?php
                            }
                            ?>
                            <div class="bg-dica">
                                <div class="categoria">
                                    <?php
                                    // Obter as categorias
                                    $categories = get_the_category();
                                    // Verificar se existem categorias
                                    if ( ! empty( $categories ) ) {
                                        // Obter a primeira categoria
                                        $categorie = current($categories);
                                        echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                                    }
                                    ?>
                                </div>
                                <div class="titulo">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </div>
                                <p>
                                    <?php echo $meta['texto_destaque']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php $i++; endwhile; endif; wp_reset_postdata(); ?>
            </div>
        </div>
        <!-- /#dicas -->

        <!-- #categorias -->
        <div id="contenthomeblog">
            <div id="categorias" class="container">
            <div id="escolha">
                <div id="escolha-label">ESCOLHA SEU ASSUNTO</div>
                <ul class="simple-tabs text-center" id="escolha-categorias">
                    <li class="categoria-1 active">Financiamento<span class="divisor"></span></li>
                    <li class="categoria-2">Fundo Perdido<span class="divisor"></span></li>
                    <li class="categoria-3">M&amp;A<span class="divisor"></span></li>
                    <li class="categoria-4">Lei do Bem</li>
                </ul>
            </div>
            <div class="clearfix"></div>
            
            <!-- .tabe-page -->
            <div id="categoria-1" class="tab-page active-page">
                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                    'cat' => 3,
                    'orderby' => 'rand',
                    'post__not_in' => $not_in,
                    'paged' => $paged
                );
                $the_query = new WP_Query( $args );
                $i = 1;

                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    $meta = get_post_meta( $post->ID, 'custom_fields', true );
                ?>
                <!-- .cat-item -->
                <div class="cat-item">
                    <?php
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail( 'destaque_home' );
                    } else {
                    ?>
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-1.png" />
                    <?php
                    }
                    ?>
                    <div class="cat-item-text">
                        <div class="categoria">
                            <?php
                            // Obter as categorias
                            $categories = get_the_category();
                            // Verificar se existem categorias
                            if ( ! empty( $categories ) ) {
                                // Obter a primeira categoria
                                $categorie = current($categories);
                                echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                            }
                            ?>
                        </div>
                        <div class="titulo">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                        <div class="texto"><?php echo $meta['texto_destaque']; ?></div>
                        <a href="<?php the_permalink(); ?>">
                            <div class="link">LEIA MAIS</div>
                        </a>
                    </div>
                </div>
                <!-- /.cat-item -->
                <?php $i++; endwhile; endif; ?>
                <!-- /.cat-item -->
            <?php wp_reset_postdata(); ?> 
            <!-- #contenthomeblog .wp-pagenavi span -->
            <div class="wp-pagenavi">
                <span>1</span>
                <a href="<?php echo get_site_url(); ?>/blog">2</a>
                <a href="<?php echo get_site_url(); ?>/blog">3</a>
                <a href="<?php echo get_site_url(); ?>/blog">4</a>
                <a href="<?php echo get_site_url(); ?>/blog">..</a>
            </div>
                
            </div>
            <!-- /.tabe-page -->
            <!-- /.tabe-page -->

            <!-- .tabe-page -->
            <div id="categoria-2" class="tab-page">
                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                    'cat' => 4,
                    'orderby' => 'rand',
                    'post__not_in' => $not_in,
                    'paged' => $paged
                );
                $the_query = new WP_Query( $args );
                $i = 1;

                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    $meta = get_post_meta( $post->ID, 'custom_fields', true );
                ?>
                <!-- .cat-item -->
                <div class="cat-item">
                    <?php
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail( 'destaque_home' );
                    } else {
                    ?>
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-1.png" />
                    <?php
                    }
                    ?>
                    <div class="cat-item-text">
                        <div class="categoria">
                            <?php
                            // Obter as categorias
                            $categories = get_the_category();
                            // Verificar se existem categorias
                            if ( ! empty( $categories ) ) {
                                // Obter a primeira categoria
                                $categorie = current($categories);
                                echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                            }
                            ?>
                        </div>
                        <div class="titulo">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                        <div class="texto"><?php echo $meta['texto_destaque']; ?></div>
                        <a href="<?php the_permalink(); ?>">
                            <div class="link">LEIA MAIS</div>
                        </a>
                    </div>
                </div>
                <!-- /.cat-item -->
                <?php $i++; endwhile; endif; ?>
            <div class="wp-pagenavi">
                <span>1</span>
                <a href="<?php echo get_site_url(); ?>/blog">2</a>
                <a href="<?php echo get_site_url(); ?>/blog">3</a>
                <a href="<?php echo get_site_url(); ?>/blog">4</a>
                <a href="<?php echo get_site_url(); ?>/blog">..</a>
            </div>
            <?php wp_reset_postdata(); ?>
                <!-- /.cat-item -->
            </div>
            <!-- /.tabe-page -->
            <!-- /.tabe-page -->

            <!-- .tabe-page -->
            <div id="categoria-3" class="tab-page">
                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                    'cat' => 6,
                    'orderby' => 'rand',
                    'post__not_in' => $not_in,
                    'paged' => $paged
                );
                $the_query = new WP_Query( $args );
                $i = 1;

                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    $meta = get_post_meta( $post->ID, 'custom_fields', true );
                ?>
                <!-- .cat-item -->
                <div class="cat-item">
                    <?php
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail( 'destaque_home' );
                    } else {
                    ?>
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-1.png" />
                    <?php
                    }
                    ?>
                    <div class="cat-item-text">
                        <div class="categoria">
                            <?php
                            // Obter as categorias
                            $categories = get_the_category();
                            // Verificar se existem categorias
                            if ( ! empty( $categories ) ) {
                                // Obter a primeira categoria
                                $categorie = current($categories);
                                echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                            }
                            ?>
                        </div>
                        <div class="titulo">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                        <div class="texto"><?php echo $meta['texto_destaque']; ?></div>
                        <a href="<?php the_permalink(); ?>">
                            <div class="link">LEIA MAIS</div>
                        </a>
                    </div>
                </div>
                <!-- /.cat-item -->
                <?php $i++; endwhile; endif; ?>
                <!-- /.cat-item -->
            <div class="wp-pagenavi">
                <span>1</span>
                <a href="<?php echo get_site_url(); ?>/blog">2</a>
                <a href="<?php echo get_site_url(); ?>/blog">3</a>
                <a href="<?php echo get_site_url(); ?>/blog">..</a>
            </div>
            <?php wp_reset_postdata(); ?>
            </div>
            <!-- /.tabe-page -->
            <!-- /.tabe-page -->

            <!-- .tabe-page -->
            <div id="categoria-4" class="tab-page">
                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                    'cat' => 5,
                    'orderby' => 'rand',
                    // 'post__not_in' => $not_in,
                    'paged' => $paged
                );
                $the_query = new WP_Query( $args );
                $i = 1;

                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    $meta = get_post_meta( $post->ID, 'custom_fields', true );
                ?>
                <!-- .cat-item -->
                <div class="cat-item">
                    <?php
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail( 'destaque_home' );
                    } else {
                    ?>
                    <img class="destaque" src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/cat-item-1.png" />
                    <?php
                    }
                    ?>
                    <div class="cat-item-text">
                        <div class="categoria">
                            <?php
                            // Obter as categorias
                            $categories = get_the_category();
                            // Verificar se existem categorias
                            if ( ! empty( $categories ) ) {
                                // Obter a primeira categoria
                                $categorie = current($categories);
                                echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                            }
                            ?>
                        </div>
                        <div class="titulo">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                        <div class="texto"><?php echo $meta['texto_destaque']; ?></div>
                        <a href="<?php the_permalink(); ?>">
                            <div class="link">LEIA MAIS</div>
                        </a>
                    </div>
                </div>
            <?php $i++; endwhile; endif; ?>
            <div class="wp-pagenavi">
                <span>1</span>
                <a href="<?php echo get_site_url(); ?>/blog">2</a>
                <a href="<?php echo get_site_url(); ?>/blog">3</a>
                <a href="<?php echo get_site_url(); ?>/blog">4</a>
                <a href="<?php echo get_site_url(); ?>/blog">..</a>
            </div>
            <?php wp_reset_postdata(); ?>
                <!-- /.cat-item -->
            </div>
            <!-- /.tabe-page -->
        </div>
        <!-- /#categorias -->
        </div>
    </div>
    <!-- /.bg-escuro -->

    <!-- .bg-azul -->
    <div class="bg-azul">
        <!-- #servicos -->
        <div id="servicos" class="container">
            <div class="row">
                <h3>SERVIÇOS</h3>
                <div class="hr-border"></div>
            </div> 
            <div class="row">
                <a href="<?php echo get_site_url(); ?>/fundo-perdido"><div class="col-lg-4">
                    <div class="servico fund-perd">
                        <h2>Fundo Perdido</h2>
                        <p>
                            <?php echo $fundo_perdido_texto; ?>
                        </p>
                    </div>
                </div></a>
                <a href="<?php echo get_site_url(); ?>/lei-do-bem"><div class="col-lg-4">
                    <div class="servico leidbem active">
                        <h2>Lei do Bem</h2>
                        <p>
                            <?php echo $lei_do_bem_texto; ?>
                        </p>
                    </div>
                </div></a>
                <a href="<?php echo get_site_url(); ?>/fusoes-e-aquisicoes"><div class="col-lg-4">
                    <div class="servico mea">
                        <h2>M&amp;A</h2>
                        <p>
                            <?php echo $mea_texto ?>
                        </p>
                    </div>
                </div></a>
            </div>
        </div>
        <!-- /#servicos -->
    </div>
    <!-- .bg-azul -->

    <!-- #idr-em-numeros -->
    <div id="idr-em-numeros" class="container">
        <h3>IDR EM NÚMEROS</h3>
        <hr>
        <div class="row desktop-idr-em-num">
            <div class="partnum col-md-4 text-center">
                <div class="upper">
                    <div class="mais"><img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/maisss.png" alt=""></div>
                    <div class="dolar">R$</div>
                    <div class="num-valor"><span id="lines1">0</span></div>
                    <div class="milhoes">Milhões</div>               
                </div><!-- upper -->
                <div class="downer">
                    <p class="lead"><?php echo $esquerda_em_num; ?></p>
                </div><!-- downer -->
            </div><!-- col -->
            <div class="partnum col-md-4 text-center">
                <div class="upper">
                    <div class="mais"><img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/maisss.png" alt=""></div>
                    <div class="dolar">R$</div>
                    <div class="num-valor"><span id="lines2">0</span></div>
                    <div class="milhoes">Milhões</div>               
                </div><!-- upper -->
                <div class="downer">
                    <p class="lead"><?php echo $centro_em_num; ?></p>
                </div><!-- downer -->
            </div><!-- col -->
            <div class="partnum col-md-4 text-center">
                <div class="upper">
                    <div class="mais"><img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/maisss.png" alt=""></div>
                    <div class="dolar">R$</div>
                    <div class="num-valor"><span id="lines3">0</span></div>
                    <div class="milhoes">Milhões</div>               
                </div><!-- upper -->
                <div class="downer">
                    <p class="lead"><?php echo $direita_em_num; ?></p>
                </div><!-- downer -->
            </div><!-- col -->
        </div>
        <div class="ipad-idr-em-num">
            <div class="row">
                <div class="col-sm-6">
                    <div class="upper">
                        <div class="mais"><img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/maisss.png" alt=""></div>
                        <div class="dolar">R$</div>
                        <div class="num-valor"><span id="lines4">0</span></div>
                        <div class="milhoes">Milhões</div>               
                    </div><!-- upper -->
                </div><!-- col-sm-6 -->
                <div class="col-sm-6">
                    <div class="downer">
                        <p class="lead"><?php echo $esquerda_em_num; ?></p>
                    </div><!-- downer -->
                </div>
            </div><!-- .row -->
            <div class="row">
                <div class="col-sm-6">
                    <div class="upper">
                        <div class="mais"><img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/maisss.png" alt=""></div>
                        <div class="dolar">R$</div>
                        <div class="num-valor"><span id="lines5">0</span></div>
                        <div class="milhoes">Milhões</div>               
                    </div><!-- upper -->
                </div><!-- col-sm-6 -->
                <div class="col-sm-6">
                    <div class="downer">
                        <p class="lead"><?php echo $centro_em_num; ?></p>
                    </div><!-- downer -->
                </div>
            </div><!-- .row -->
            <div class="row">
                <div class="col-sm-6">
                    <div class="upper">
                        <div class="mais"><img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/maisss.png" alt=""></div>
                        <div class="dolar">R$</div>
                        <div class="num-valor"><span id="lines6">0</span></div>
                        <div class="milhoes">Milhões</div>               
                    </div><!-- upper -->
                </div><!-- col-sm-6 -->
                <div class="col-sm-6">
                    <div class="downer">
                        <p class="lead"><?php echo $direita_em_num; ?></p>
                    </div><!-- downer -->
                </div>
            </div><!-- .row -->
        </div>
    </div>

    <!-- /#idr-em-numeros -->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-center" style="font-size: 1.5em !important;font-weight: bold;" id="myModalLabel">Deixe seu contato e enviaremos as melhores opções de financiamento para a sua empresa</h4>
          </div>
          <div class="modal-body">
            <?php echo do_shortcode( '[contact-form-7 id="534" title="Financiamento"]' ); ?>
          </div>
        </div>
      </div>
    </div>


<?php get_footer(); ?>
