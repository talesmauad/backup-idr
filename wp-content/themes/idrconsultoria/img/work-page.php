<?php
/**
 * Template Name: Trabalhe Conosco
 *
 * @package WordPress
 * @subpackage IDR Consultoria
 */
?>
<?php get_header(); ?>

    <div id="container" class="container">
        <div id="faixa-interna">
            <div>Trabalhe conosco</div>
        </div>
        <div id="breadcrumb">
            idr > <?php the_title(); ?>
        </div>

        <div id="content" class="work-form">

            <h3>Procuramos profissionais de excelência na área de engenharia, finanças e administração.</h3>

            <?php echo do_shortcode( '[contact-form-7 id="405" title="Trabalhe Conosco"]' ); ?>

        </div><!-- #content -->
        <div id="faq-sidebar">
            <div class="tit-submenu">Leia também</div>
            <ul>
                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                );
                $the_query = new WP_Query( $args );
                $i = 1;

                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    $meta = get_post_meta( $post->ID, 'custom_fields', true );
                ?>
                <li>
                    <a href="<?php the_permalink(); ?>" class="link-img">
                        <?php
                        if ( has_post_thumbnail() ) {
                            the_post_thumbnail( 'destaque_lateral' );
                        }
                        ?>
                    </a>
                    <a href="<?php the_permalink(); ?>" class="categoria">
                        <?php
                        // Obter as categorias
                        $categories = get_the_category();
                        // Verificar se existem categorias
                        if ( ! empty( $categories ) ) {
                            // Obter a primeira categoria
                            $categorie = current($categories);
                            echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                        }
                        ?>
                    </a>
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    <div class="clearfix"></div>
                    <a href="<?php the_permalink(); ?>" class="link-ler-mais">Leia mais</a>
                </li>
                <?php $i++; endwhile; endif; wp_reset_postdata(); ?>
            </ul>
        </div>
    </div><!-- #container -->

<?php get_footer(); ?>

