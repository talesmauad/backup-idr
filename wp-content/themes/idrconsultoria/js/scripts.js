jQuery(document).ready(function($) {
    $(document).on('click', 'a[href^="#"]', function(e) {
        // target element id
        var id = jQuery(this).attr('href');

        if(id !== '#') {
                // target element
                var $id = jQuery(id);
                if ($id.length === 0) {
                    return;
                }

                // prevent standard hash navigation (avoid blinking in IE)
                e.preventDefault();

                // top position relative to the document
                var pos = $id.offset().top;

                // animated top scrolling
                jQuery('body, html').animate({scrollTop: pos}, 1500);
        }

    });

    $(document).on('click', 'li.dropdown',function(event){
      $('ul.dropdown-menu').hide();
      $('li.slicknav_parent').removeClass('slicknav_open').addClass('slicknav_collapsed');
      $(this).removeClass('slicknav_collapsed').addClass('slicknav_open');
      $(this).find('.dropdown-menu').show();
    });

    $(function(){
        $('#second-menu').slicknav();
    });
    // banner principal escolhas dinâmicas
    $('.opcao1').on('click', function(){
        $('.opcao1').removeClass('active');
        $(this).addClass('active');
    });
    $('.opcao2').on('click', function(){
        $('.opcao2').removeClass('active');
        $(this).addClass('active');
    });
    $('.opcao3').on('click', function(){
        $('.opcao3').removeClass('active');
        $(this).addClass('active');
    });

      // posts dinâmicos page-home
    $('.categoria-1').on('click', function(){
        $('.categoria-2').removeClass('active');
        $('.categoria-3').removeClass('active');
        $('.categoria-4').removeClass('active');
        $('.tab-page').removeClass('active-page');
        $('.categoria-1').addClass('active');
        $('#categoria-1').addClass('active-page');

    });
    $('.categoria-2').on('click', function(){
        $('.categoria-1').removeClass('active');
        $('.categoria-3').removeClass('active');
        $('.categoria-4').removeClass('active');
        $('.tab-page').removeClass('active-page');
        $('.categoria-2').addClass('active');
        $('#categoria-2').addClass('active-page');

    });
    $('.categoria-3').on('click', function(){
        $('.categoria-2').removeClass('active');
        $('.categoria-1').removeClass('active');
        $('.categoria-4').removeClass('active');
        $('.tab-page').removeClass('active-page');
        $('.categoria-3').addClass('active');
        $('#categoria-3').addClass('active-page');

    });
    $('.categoria-4').on('click', function(){
        $('.categoria-2').removeClass('active');
        $('.categoria-3').removeClass('active');
        $('.categoria-1').removeClass('active');
        $('.tab-page').removeClass('active-page');
        $('.categoria-4').addClass('active');
        $('#categoria-4').addClass('active-page');

    });
    $('.but-um').on('click', function(){
        console.log('ckicou');
        $('#submenu2').css({ 'display': "none !important" });
        $('#submenu3').css({ 'display': "none !important" });
        $('#submenu4').css({ 'display': "none !important" });
    });

    $('.servico').hover(function(){
        $('.servico').removeClass('active');
        $(this).addClass('active');
    });

    $('.opcao1').click(function(e){
        var selecionado = $(this).attr('data-value');
        $('input[value="'+selecionado+'"]').prop('checked', true);
    });
    $('.opcao2').click(function(e){
        var selecionado = $(this).attr('data-value');
        $('input[value="'+selecionado+'"]').prop('checked', true);
    });
    $('.opcao3').click(function(e){
        var selecionado = $(this).attr('data-value');
        $('input[value="'+selecionado+'"]').prop('checked', true);
    });

    $('.modal-body').on('click', function(){
        $('.escondido-b').removeClass('escondido');
        $('.row').removeClass('escondido');
    });
    var div_top;
    if($('#idr-em-numeros').length) {
      div_top = $('#idr-em-numeros').offset().top;
    }
    var a = 1;
    $(window).scroll(function(){
        if($(window).scrollTop() > div_top - 600){
           $('#lines1').animateNumber({ number: 500 });
           $('#lines2').animateNumber({ number: 100 });
           $('#lines3').animateNumber({ number: 20 });
           div_top = 100000000;
        }
        if(a > 0){
           $('#lines4').animateNumber({ number: 500 });
           $('#lines5').animateNumber({ number: 100 });
           $('#lines6').animateNumber({ number: 20 });
           a = -1000;
        }
    });
});
