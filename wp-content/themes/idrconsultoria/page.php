<?php
/*ADVANCED CUSTOM FIELDS*/
/* FICHA TÉCNICA */
$titulo1        = get_field('titulo_1');
$texto1         = get_field('texto_1');
$titulo2        = get_field('titulo_2');
$texto2         = get_field('texto_2');
$texto_box      = get_field('texto_box');
$url_destino    = get_field('url_destino');

?>

<?php the_post(); get_header(); ?>

    <div id="faixa-interna">
        <div class="container"><?php the_title(); ?></div>
    </div>
    <div id="breadcrumb">
        <div class="container">idr > <?php the_title() ?></div>
    </div>
    <div id="container" class="container">

        <div id="content">

            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <h1 class="entry-title"><?php the_title(); ?></h1>

                <div class="entry-content">
                    <?php if ( has_post_thumbnail() ) { the_post_thumbnail(get_the_ID()); } ?>
                    <?php the_content(); ?>
                </div><!-- .entry-content -->

            </div><!-- #post-<?php the_ID(); ?> -->

            <?php if(!empty($titulo1)): ?>
            <div id="ficha-tecnica">
                <div class="ficha-tecnica-tit">ficha técnica</div>
                <div class="ficha-tecnica-item">
                    <div class="ficha-item-tit"><?php echo $titulo1 ?></div>
                    <?php echo $texto1 ?>
                </div>
                <div class="ficha-tecnica-item">
                    <div class="ficha-item-tit"><?php echo $titulo2 ?></div>
                    <?php echo $texto2 ?>
                </div>
            </div>
            <?php endif ?>

            <div id="fale-consultor">
                <div class="tit1">Fale com um consultor</div>
                <div class="tit2">Deixe seu telefone e ligaremos em breve</div>
                <?php echo do_shortcode( '[contact-form-7 id="446" title="Fale com um consultor"]' ); ?>
            </div>

        </div><!-- #content -->

        <div id="page-sidebar">
            <?php
            $title_menu = (
                $post->post_parent == 76
                || $post->post_parent == 92
                || $post->post_parent == 103
                || $post->post_parent == 111
            ) ? get_the_title($post->post_parent): $post->post_title;
            $id_menu = (
                $post->post_parent == 76
                || $post->post_parent == 92
                || $post->post_parent == 103
                || $post->post_parent == 111
            ) ? $post->post_parent : $post->ID;
            ?>
            <div class="tit-submenu"><?php echo $title_menu; ?></div>
            <?php
                $args = array(
                    'post_type' => 'page',
                    'posts_per_page' => -1,
                    'orderby' => 'rand',
                    'post_parent' => $id_menu,
                    'order' => 'ASC',
                    'orderby' => 'menu_order'
                );
                $the_query = new WP_Query( $args );
            ?>
            <ul>
                <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <li>
                      <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                      <?php $pageid = get_the_ID(); ?>
                      <?php $children = get_children(array('post_parent'=>$pageid, 'post_type' => 'page')); ?>
                      <?php if(!empty($children)): ?>
                        <ul>
                          <?php foreach($children as $child) : ?>
                            <li><a href="<?php echo get_permalink($child); ?>"><?php echo get_the_title($child); ?></a></li>
                          <?php endforeach; ?>
                        </ul>
                      <?php endif; ?>
                    </li>

                <?php endwhile; ?>
                <?php endif; wp_reset_postdata(); ?>
            </ul>
            <a class="textobox" href="<?php echo $url_destino; ?>">
                <p ><?php echo $texto_box;?></p>
            </a>
        </div>

    </div><!-- #container -->

<?php get_footer(); ?>
