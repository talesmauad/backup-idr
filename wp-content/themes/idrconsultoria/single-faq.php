﻿<?php the_post(); get_header(); ?>

    <div id="faixa-interna">
        <div class="container">Faq</div>
    </div>
    <div id="breadcrumb">
        <div class="container">idr > faq</div>
    </div>
    <div id="container" class="container faaaq">

        <div id="content">

            <?php
            // Dados do post principal
            // Id do post principal
            $main_post_id = get_the_ID();
            // Obter as categorias
            $categories = get_the_category();
            // Verificar se existem categorias
            if ( ! empty( $categories ) ) {
                // Obter a primeira categoria
                $categorie = current($categories);
                // Obter o código da primeira categoria
                $cat_ID = $categorie->cat_ID;
            }
            ?>

            <!-- #categorias -->
            <div id="categorias" class="container">
                <h2 class="text-center tirar-desk" style="font-weight: bold;">FAQ</h2>
                <h3 class="text-center">Escolha seu assunto</h3>
                <div id="escolha">
                    <ul class="" id="escolha-categorias">
                        <li class="esp categoria-1<?php echo ($cat_ID == 3) ? ' active':''; ?>">Financiamento<span class="divisor"></span></li>
                        <li class="esp categoria-2<?php echo ($cat_ID == 4) ? ' active':''; ?>">Fundo perdido<span class="divisor"></span></li>
                        <li class="esp categoria-3<?php echo ($cat_ID == 6) ? ' active':''; ?>">M&amp;A<span class="divisor"></span></li>
                        <li class="esp categoria-4<?php echo ($cat_ID == 5) ? ' active':''; ?>">Lei do Bem</li>
                    </ul>
                </div>
                <div class="clearfix"></div>

                <!-- .tabe-page -->
                <div id="categoria-1" class="tab-page<?php echo ($cat_ID == 3) ? ' active-page':''; ?>">

                    <div class="accordion-faq">
                        <?php
                            $args = array(
                                'post_type' => 'faq',
                                'posts_per_page' => 100,
                                'cat' => 3,
                            );
                            $the_query = new WP_Query( $args );
                            $i = 1;
                        ?>
                        <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">
                                    <span><?php echo $i++; ?> )</span>
                                    <a href="<?php the_permalink(); ?>#virpraca" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
                                </h4>
                                <div class="accordion-content" <?php echo (get_the_ID() == $main_post_id) ? 'id="virpraca"':'style="display:none;"'; ?>>
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        <?php endwhile; else : ?>
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">
                                    <a href="#">Nenhum item encontrado</a>
                                </h4>
                            </div>
                        <?php endif; wp_reset_postdata(); ?>
                    </div>

                </div>
                <!-- /.tabe-page -->

                <!-- .tabe-page -->
                <div id="categoria-2" class="tab-page<?php echo ($cat_ID == 4) ? ' active-page':''; ?>">

                    <div class="accordion-faq">
                        <?php
                            $args = array(
                                'post_type' => 'faq',
                                'posts_per_page' => 8, /* wp_pagenavi */
                                'cat' => 4,
                            );
                            $the_query = new WP_Query( $args );
                            $i = 1;
                        ?>
                        <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">
                                    <span><?php echo $i++; ?> )</span>
                                    <a href="<?php the_permalink(); ?>#virpraca" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
                                </h4>
                                <div class="accordion-content" <?php echo (get_the_ID() == $main_post_id) ? 'id="virpraca"':'style="display:none;"'; ?>>
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        <?php endwhile; else : ?>
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">
                                    <a href="javascript:;">Nenhum item encontrado</a>
                                </h4>
                            </div>
                        <?php endif; wp_reset_postdata(); ?>
                    </div>
                </div>
                <!-- /.tabe-page -->

                <!-- .tabe-page -->
                <div id="categoria-3" class="tab-page<?php echo ($cat_ID == 6) ? ' active-page':''; ?>">

                    <div class="accordion-faq">
                        <?php
                            $args = array(
                                'post_type' => 'faq',
                                'posts_per_page' => 8,
                                'cat' => 6,
                            );
                            $the_query = new WP_Query( $args );
                            $i = 1;
                        ?>
                        <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">
                                    <span><?php echo $i++; ?> )</span>
                                    <a href="<?php the_permalink(); ?>#virpraca" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
                                </h4>
                                <div class="accordion-content" <?php echo (get_the_ID() == $main_post_id) ? 'id="virpraca"':'style="display:none;"'; ?>>
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        <?php endwhile; else : ?>
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">
                                    <a href="<?php the_permalink(); ?>">Nenhum item encontrado</a>
                                </h4>
                            </div>
                        <?php endif; wp_reset_postdata(); ?>
                    </div>

                </div>
                <!-- /.tabe-page -->

                <!-- .tabe-page -->
                <div id="categoria-4" class="tab-page<?php echo ($cat_ID == 5) ? ' active-page':''; ?>">

                    <div class="accordion-faq">
                        <?php
                            $args = array(
                                'post_type' => 'faq',
                                'posts_per_page' => 8,
                                'cat' => 5,
                            );
                            $the_query = new WP_Query( $args );
                            $i = 1;
                        ?>
                        <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">
                                    <span><?php echo $i++; ?> )</span>
                                    <a href="<?php the_permalink(); ?>#virpraca" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
                                </h4>
                                <div class="accordion-content" <?php echo (get_the_ID() == $main_post_id) ? 'id="virpraca"':'style="display:none;"'; ?>>
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        <?php endwhile; else : ?>
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">
                                    <a href="<?php the_permalink(); ?>">Nenhum item encontrado</a>
                                </h4>
                            </div>
                        <?php endif; wp_reset_postdata(); ?>
                    </div>

                </div>
                <!-- /.tabe-page -->

            </div>
            <!-- /#categorias -->

            <div id="fale-consultor">
                <div class="tit1">Fale com um consultor</div>
                <div class="tit2">Deixe seu telefone e ligaremos em breve</div>
                <form action="javascript:;">
                    <input type="text" id="tel" name="tel" placeholder="Telefone" />
                    <input id="submit" type="submit" value="Enviar" />
                </form>
            </div>

        </div><!-- #content -->

        <div id="faq-sidebar">
            <div class="tit-submenu">Leia também</div>
            <ul>
                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                );
                $the_query = new WP_Query( $args );
                $i = 1;

                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    $meta = get_post_meta( $post->ID, 'custom_fields', true );
                ?>
                <li>
                    <a href="<?php the_permalink(); ?>" class="link-img">
                        <?php
                        if ( has_post_thumbnail() ) {
                            the_post_thumbnail( 'destaque_lateral' );
                        }
                        ?>
                    </a>
                    <a href="<?php the_permalink(); ?>" class="categoria">
                        <?php
                        // Obter as categorias
                        $categories = get_the_category();
                        // Verificar se existem categorias
                        if ( ! empty( $categories ) ) {
                            // Obter a primeira categoria
                            $categorie = current($categories);
                            echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                        }
                        ?>
                    </a>
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    <div class="clearfix"></div>
                    <a href="<?php the_permalink(); ?>" class="link-ler-mais">Leia mais</a>
                </li>
                <?php $i++; endwhile; endif; wp_reset_postdata(); ?>
            </ul>
        </div>

    </div><!-- #container -->

<?php get_footer(); ?>
