<?php the_post(); get_header(); ?>

    <div id="faixa-interna">
        <div class="container">Blog</div>
    </div>
    <div id="breadcrumb">
        <div class="container">idr > blog > <?php the_title(); ?></div>
    </div>
    <div id="container" class="container">

        <div id="content">

            <h1 style="font-weight: bold; font-size: 40px;" class="entry-title"><?php the_title(); ?></h1">
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div id="post-category">
                <?php
                // Obter as categorias
                $categories = get_the_category();

                // Verificar se existem categorias
                if ( ! empty( $categories ) ) {
                    // Obter a primeira categoria
                    $categorie = current($categories);
                    echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                }
                ?>

                </div>

                <div class="entry-content">
                    <div class="thumbnail-blog" style="padding:50px;">
                        <?php the_post_thumbnail(get_the_ID()); ?>
                    </div>
                    <?php the_content(); ?>
                </div><!-- .entry-content -->

            </div><!-- #post-<?php the_ID(); ?> -->

        </div><!-- #content -->

        <div id="post-footer">
            <!-- #dicas -->
            <div id="dicas" class="container" style="margin-bottom:60px;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="dica">
                            <h3 style="color: #337ab7; text-align:left;">Veja também</h3>
                            <div class="hr-border"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php
                    $args = array(
                        'post_type' => 'post',
                        'posts_per_page' => 3,
                        'orderby' => 'rand',
                    );
                    $the_query = new WP_Query( $args );
                    $i = 1;
                    $not_in = array();

                    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                        $meta = get_post_meta( $post->ID, 'custom_fields', true );
                        $not_in[] = $post->ID;
                    ?>
                        <div class="col-lg-4">
                            <div class="dica">
                                <?php
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail( 'dicas_tutoriais' );
                                } else {
                                ?>
                                    <img src="<?php echo get_site_url(); ?>/wp-content/themes/idrconsultoria/img/dica-<?php echo $i; ?>.png" />
                                <?php
                                }
                                ?>
                                <div class="bg-dica" style="background: #e5e5e5;">
                                    <div class="categoria" style="color: #000;">
                                        <?php
                                        // Obter as categorias
                                        $categories = get_the_category();
                                        // Verificar se existem categorias
                                        if ( ! empty( $categories ) ) {
                                            // Obter a primeira categoria
                                            $categorie = current($categories);
                                            echo ($categorie->cat_ID != 1) ? $categorie->cat_name: '';
                                        }
                                        ?>
                                    </div>
                                    <div class="titulo">
                                        <a href="<?php the_permalink(); ?>" style="color:#36829f;"><?php the_title(); ?></a>
                                    </div>
                                    <p style="color: #000;">
                                        <?php echo $meta['texto_destaque']; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php $i++; endwhile; endif; wp_reset_postdata(); ?>
                </div>
            </div>
            <!-- /#dicas -->

            <!-- fale-consultor -->
            <div id="fale-consultor">
                <div class="tit1">Fale com um consultor</div>
                <div class="tit2">Deixe seu telefone e ligaremos em breve</div>
                <?php echo do_shortcode( '[contact-form-7 id="446" title="Fale com um consultor"]' ); ?>
            </div>
            <!-- /fale-consultor -->
        </div>

    </div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
